package ru.vexcore.cms.repository.news;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.news.BlogTag;

import java.util.List;

public interface BlogTagRepository extends CrudRepository<BlogTag,Long> {
    BlogTag getByName(String name);
    List<BlogTag> findAllByNameContaining(String term);
}
