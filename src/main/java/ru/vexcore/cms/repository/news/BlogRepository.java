package ru.vexcore.cms.repository.news;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.news.Blog;

import java.util.List;

public interface BlogRepository extends CrudRepository<Blog,Long> {
    List<Blog> findAll(Pageable pageable);
    Blog getBlogById(Long id);

    List<Blog> findAllByBodyContains(String term);
}
