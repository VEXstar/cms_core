package ru.vexcore.cms.repository.news;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.news.BlogComment;

public interface BlogCommentRepository extends CrudRepository<BlogComment,Long> {
    BlogComment getById(Long id);
}
