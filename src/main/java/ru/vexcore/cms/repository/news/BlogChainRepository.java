package ru.vexcore.cms.repository.news;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.news.BlogTagsChain;

import java.util.List;

public interface BlogChainRepository extends CrudRepository<BlogTagsChain, Long> {
    List<BlogTagsChain> findAllByBlogId(Long id);
}
