package ru.vexcore.cms.repository;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.UserInfo;

public interface UserInfoRepository extends CrudRepository<UserInfo,String> {
    UserInfo getUserInfoBySub(String sub);
}
