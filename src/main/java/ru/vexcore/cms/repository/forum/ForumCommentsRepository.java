package ru.vexcore.cms.repository.forum;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumComment;

import java.util.List;

public interface ForumCommentsRepository extends CrudRepository<ForumComment,Long> {
    List<ForumComment> findAllByUserSub(String userSub);
    List<ForumComment> findAllByUserInfo(UserInfo userInfo);
    List<ForumComment> findAllByPostID(Long id, Pageable pageable);
    ForumComment getById(Long id);
    Integer countAllByPostID(Long id);

}
