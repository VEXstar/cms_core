package ru.vexcore.cms.repository.forum;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.forum.ForumEditor;

import java.util.List;

public interface ForumChainPostUserRepository extends CrudRepository<ForumEditor,Long> {
    List<ForumEditor> getAllByUserSub(String sub);
    List<ForumEditor> getAllByPostID(Long id);
}
