package ru.vexcore.cms.repository.forum;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.forum.ForumPost;
import ru.vexcore.cms.model.forum.ForumTopic;

import java.util.List;

public interface ForumPostRepository extends CrudRepository<ForumPost,Long> {
    ForumPost getById(Long id);
    List<ForumPost> findAllByTopicIdAndOrderByPostedTimeDesc(Long topicID);
    List<ForumPost> findAllByTopic(ForumTopic topicID);

}
