package ru.vexcore.cms.repository.forum;


import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.forum.ForumTopic;

import java.util.List;

public interface ForumTopicRepository extends CrudRepository<ForumTopic,Long> {
    ForumTopic getById(Long id);
    List<ForumTopic> findAllByTitleContaining(String title);
    List<ForumTopic> getAllByRootTopicIdIsNull();
}
