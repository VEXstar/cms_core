package ru.vexcore.cms.repository.additional.poll;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.additional.poll.PollAnswer;

public interface PollAnswerRepository extends CrudRepository<PollAnswer,Long> {
    PollAnswer getBySubAndPollId(String sub,Long pollId);
}
