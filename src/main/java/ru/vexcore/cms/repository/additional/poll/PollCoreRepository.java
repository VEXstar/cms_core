package ru.vexcore.cms.repository.additional.poll;


import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.additional.poll.PollCore;

public interface PollCoreRepository extends CrudRepository<PollCore,Long> {
    PollCore getById(Long id);

}
