package ru.vexcore.cms.repository.additional.poll;

import org.springframework.data.repository.CrudRepository;
import ru.vexcore.cms.model.additional.poll.PollCheck;

public interface PollCheckRepository extends CrudRepository<PollCheck,Long> {
}
