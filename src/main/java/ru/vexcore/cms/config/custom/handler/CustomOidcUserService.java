package ru.vexcore.cms.config.custom.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.logic.user.UserDataWorkerService;
import ru.vexcore.cms.logic.user.UserRepositoryService;
import ru.vexcore.cms.model.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomOidcUserService extends OidcUserService {

    @Autowired
    UserDataWorkerService manager;

    private List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    public CustomOidcUserService() {
        super();

    }

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {
        DefaultOidcUser oidcUser = (DefaultOidcUser)super.loadUser(userRequest);

        Object[] objects = oidcUser.getAuthorities().toArray();
        for(Object authory : objects){
            if(authory instanceof OidcUserAuthority)
                grantedAuthorities.add((OidcUserAuthority)authory);
            else if(authory instanceof SimpleGrantedAuthority)
                grantedAuthorities.add((SimpleGrantedAuthority)authory);
        }
        UserInfo userInfo = manager.loadOrCreateUser(oidcUser);
        userInfo.getRoles().forEach(x->grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+x)));
        Map<String,Object> trah = new HashMap<>();
        trah.put("data",userInfo);
        OidcUserInfo oidcUserInfo = new OidcUserInfo(trah);

        DefaultOidcUser customUser = new DefaultOidcUser(grantedAuthorities,oidcUser.getIdToken(),oidcUserInfo);
        return customUser;
    }


}
