package ru.vexcore.cms.config.custom.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import ru.vexcore.cms.model.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

@Component
public class VerifyAccessInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth==null||(auth.getPrincipal() instanceof String&&auth.getPrincipal().toString().equals("anonymousUser")))
            return true;
        Set<GrantedAuthority> authorities = new HashSet<>();
        ((UserInfo)((DefaultOidcUser)auth.getPrincipal()).getClaim("data")).getRoles().forEach(x->authorities.add(new SimpleGrantedAuthority("ROLE_"+x)));
        Authentication newAuth = null;
        if (auth.getClass() == OAuth2AuthenticationToken.class) {
            OAuth2User principal = ((OAuth2AuthenticationToken)auth).getPrincipal();
            if (principal != null) {
                newAuth = new OAuth2AuthenticationToken(principal, authorities,(((OAuth2AuthenticationToken)auth).getAuthorizedClientRegistrationId()));
            }
        }
        SecurityContextHolder.getContext().setAuthentication(newAuth);
        return true;
    }

}