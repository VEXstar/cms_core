package ru.vexcore.cms.logic.additional;

import org.apache.tomcat.jni.Poll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.additional.poll.PollAnswer;
import ru.vexcore.cms.model.additional.poll.PollCheck;
import ru.vexcore.cms.model.additional.poll.PollCore;
import ru.vexcore.cms.repository.additional.poll.PollAnswerRepository;
import ru.vexcore.cms.repository.additional.poll.PollCheckRepository;
import ru.vexcore.cms.repository.additional.poll.PollCoreRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class PollService {

    @Autowired
    PollAnswerRepository answerRepository;

    @Autowired
    PollCoreRepository coreRepository;

    @Autowired
    PollCheckRepository checkRepository;

    public ResponseEntity<String> answerPoll(Long pollID, Long checkId, UserInfo userInfo){
        PollCore core = coreRepository.getById(pollID);
        if(core.getLike()==null||core.getLike())
            answerAsLike(core,checkId,userInfo);
        else if(!answerAsPoll(core,checkId,userInfo))
            return Utils.responseGenerator(false,200,"already polled or poll expired or check not found");
        return Utils.responseGenerator(true);
    }
    private void answerAsLike(PollCore pollCore,Long checkId,UserInfo info){
        for (PollAnswer answer : pollCore.getAnswers()) {
            if(answer.getSub().equals(info.getSub())){
                if(checkId!=null){
                    answer.setCheckID(checkId);
                    answerRepository.save(answer);
                }
                else
                    answerRepository.delete(answer);
                return;
            }
        }
        PollAnswer answer = new PollAnswer();
        answer.setCheckID(checkId);
        answer.setPollId(pollCore.getId());
        answer.setSub(info.getSub());
        answerRepository.save(answer);
    }
    private Boolean answerAsPoll(PollCore pollCore,Long checkId,UserInfo info){
        for (PollAnswer answer : pollCore.getAnswers()) {
            if((pollCore.getExpire()!=null&&pollCore.getExpire().after(Utils.nowTimeStamp()))||checkId==null||answer.getSub().equals(info.getSub()))
                return false;
        }
        PollAnswer answer = new PollAnswer();
        answer.setCheckID(checkId);
        answer.setPollId(pollCore.getId());
        answer.setSub(info.getSub());
        answerRepository.save(answer);
        return true;
    }



    public ResponseEntity<String> createPoll(String mood, List<String> checks,Timestamp expireTime,Boolean isLike){
        PollCore core = new PollCore();
        core.setBody(mood);
        core.setExpire(expireTime);
        core.setLike(isLike);
        PollCore save = coreRepository.save(core);
        Long id = save.getId();
        if(!isLike){
            List<PollCheck> checkList = new ArrayList<>();
            for(String s:checks){
                PollCheck check = new PollCheck();
                check.setPollId(id);
                check.setName(s);
            }
            checkRepository.saveAll(checkList);
        }
        return Utils.responseGenerator(save);
    }

    public ResponseEntity<String> getPoll(Long id){
        PollCore byId = coreRepository.getById(id);
        if(byId.getLike()==null||byId.getLike())
            return Utils.responseGenerator(handleAsLikePost(byId));
        return Utils.responseGenerator(handleAsNormalPoll(byId));
    }

    private PollCore handleAsLikePost(PollCore pollCore){
        HashSet<PollAnswer> answers = pollCore.getAnswers();
        pollCore.setChecks(new HashSet<>());
        answers.forEach(x->{
            x.setPollCore(null);
            pollCore.incrementStatistic(x.getCheckID());
            pollCore.getChecks().add(x.getPollCheck());
        });
        return pollCore;
    }
    private PollCore handleAsNormalPoll(PollCore pollCore){
        HashSet<PollAnswer> answers = pollCore.getAnswers();
        pollCore.getChecks().forEach(x->x.setPollCore(null));
        answers.forEach(x->{
            x.setPollCore(null);
            pollCore.incrementStatistic(x.getCheckID());
        });
        return pollCore;
    }


}
