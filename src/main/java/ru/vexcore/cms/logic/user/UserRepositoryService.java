package ru.vexcore.cms.logic.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.model.UserInfo;

import java.util.*;

@Service
public class UserRepositoryService {

    @Autowired
    private SessionRegistry sessionRegistry;

    public UserInfo getUserBy(String sub){
        for(Object x : sessionRegistry.getAllPrincipals()){
            UserInfo usf = ((DefaultOidcUser)x).getUserInfo().getClaim("data");
            if(usf.getSub().equals(sub)){
                return usf;
            }
        }
        return null;
    }

    public List<UserInfo> getAll(){
       List<UserInfo> allUsers = new ArrayList<>();
       sessionRegistry.getAllPrincipals().forEach(x->allUsers.add(((DefaultOidcUser)x).getUserInfo().getClaim("data")));
       return allUsers;
    }


}
