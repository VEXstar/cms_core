package ru.vexcore.cms.logic.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.role.RoleUtils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.repository.UserInfoRepository;

import javax.rmi.CORBA.Util;
import java.util.*;

@Service
public class UserDataWorkerService {

    @Autowired
    UserInfoRepository userInfoRepository;
    @Autowired
    UserRepositoryService repositoryService;

    public UserInfo loadOrCreateUser(DefaultOidcUser user){
        UserInfo userInfo = userInfoRepository.getUserInfoBySub(user.getAttributes().get("sub").toString());
        if(userInfo == null){
            List<String> rolesInit = new ArrayList<>();
            rolesInit.add("GUEST");
            userInfo = new UserInfo(user.getAttributes().get("sub").toString(),
                    null,user.getAttributes().get("name").toString(),
                    user.getAttributes().get("picture").toString(),user.getAttributes().get("email").toString(),rolesInit);
            userInfoRepository.save(userInfo);
        }
        return userInfo;
    }
    public ResponseEntity<String> updateDataUser(String sub, String nickName, String avatar){
        UserInfo usr = userInfoRepository.getUserInfoBySub(sub);
        if(usr==null)
            return Utils.responseGenerator(false,404,"user not found");
        if(nickName!=null){
            repositoryService.getUserBy(sub).setNickName(nickName);
            usr.setNickName(nickName);
        }
        if(avatar!=null){
            repositoryService.getUserBy(sub).setAvatar(avatar);
            usr.setAvatar(avatar);
        }
        return Utils.responseGenerator(userInfoRepository.save(usr));
    }
    public enum ROLE_ACTION {DELETE,UPDATE,SET};


    public ResponseEntity<String> editRoles(String sub,ROLE_ACTION action,String role){
        UserInfo usr = userInfoRepository.getUserInfoBySub(sub);
        if(usr.getRoles().contains("SYSADMIN"))
            throw new RuntimeException("Cant change roles for a System Administrator");

        List<String> roles = new ArrayList<>();
        if(!RoleUtils.rolesValidate(role,roles))
            return Utils.responseGenerator(false,400,"roles not found");

        if(action == ROLE_ACTION.DELETE){
            repositoryService.getUserBy(sub).removeRoles(roles);
            usr.removeRoles(roles);
        }
        else if( action == ROLE_ACTION.SET){
            repositoryService.getUserBy(sub).setRoles(roles);
            usr.setRoles(roles);
        }
        else if( action == ROLE_ACTION.UPDATE){
            repositoryService.getUserBy(sub).addRoles(roles);
            usr.addRoles(roles);
        }
        return Utils.responseGenerator(userInfoRepository.save(usr));
    }
    public ResponseEntity<String> getBothUser(String sub){
        List<UserInfo> usr = new ArrayList<>();
        usr.add(repositoryService.getUserBy(sub));
        usr.add(userInfoRepository.getUserInfoBySub(sub));
        return Utils.responseGenerator(usr);
    }

//
//
//    public void createNewUser(DefaultOidcUser user){
//        jdbcTemplate.execute(String.format("INSERT INTO public.employee(\n" +
//                "\tsub, name, authory, avatar, email)\n" +
//                "\tVALUES ('%s', '%s', ARRAY['GUEST'], '%s', '%s');",user.getAttributes().get("sub").toString(),user.getAttributes().get("name").toString(),user.getAttributes().get("picture").toString(),user.getAttributes().get("email").toString()));
//    }
//    public boolean isNewUser(String sub){
//        return 0 == jdbcTemplate.queryForObject(String.format("SELECT COUNT(*) FROM employee WHERE sub LIKE '%s'",sub),int.class);
//    }
//
//    private UserInfo loadUser(String sub){
//        Map<String, Object> result = jdbcTemplate.queryForMap(String.format("SELECT * FROM employee WHERE sub LIKE '%s'", sub));
//        return new UserInfo(result.get("sub").toString(),
//                result.get("nick_name").toString(),
//                result.get("name").toString(),
//                result.get("avatar").toString(),
//                result.get("email").toString(),
//                parseAthories(result.get("authory").toString())
//                );
//    }
//    private List<String> parseAthories(String resp){
//      String[] pre = resp.replace("{","").replace("}","").replace(" ","").split(",");
//      List<String> lst = new ArrayList<>();
//      Collections.addAll(lst, pre);
//      return lst;
//    }

}
