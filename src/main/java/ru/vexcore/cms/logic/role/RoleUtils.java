package ru.vexcore.cms.logic.role;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.enums.ROLES;
import ru.vexcore.cms.model.UserInfo;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component("RoleUtils")
@Service
public class RoleUtils {


    private  static Set<String> allRoles = new HashSet<>();

    @PostConstruct
    public void constructor(){
        Arrays.stream(ROLES.values()).forEach(x->allRoles.add(x.name()));
    }

    private static String rolesGenerator(ROLES ... roles){
        List<String> rolesList = Arrays.stream(roles).map(Enum::name).collect(Collectors.toList());
        return  String.join(", ",rolesList);
    }


    public static boolean userIsAdmin(UserInfo info){
        List<String> roles = info.getRoles();
        return roles.contains(ROLES.ADMIN.name())||roles.contains(ROLES.OPERATOR.name())||roles.contains(ROLES.SYSADMIN);
    }


    public static String isViewForum(){
        return rolesGenerator(ROLES.ADMIN,ROLES.OPERATOR,ROLES.USER,ROLES.PUBLICATOR,ROLES.SYSADMIN,ROLES.MODERATOR,ROLES.GONID);
    }
    public static String canWtiteForum(){
        return rolesGenerator(ROLES.ADMIN,ROLES.OPERATOR,ROLES.PUBLICATOR,ROLES.SYSADMIN,ROLES.MODERATOR,ROLES.GONID,ROLES.USER);
    }
    public static String canModerateForum(){
        return rolesGenerator(ROLES.ADMIN,ROLES.OPERATOR,ROLES.PUBLICATOR,ROLES.SYSADMIN,ROLES.MODERATOR);
    }
    public static String canPublicateNews(){
        return rolesGenerator(ROLES.ADMIN,ROLES.OPERATOR,ROLES.PUBLICATOR,ROLES.SYSADMIN,ROLES.MODERATOR,ROLES.GONID);
    }
    public static String adminRoles(){
        return rolesGenerator(ROLES.ADMIN,ROLES.OPERATOR,ROLES.SYSADMIN);
    }
    public static String isAnonymous(){
        return rolesGenerator(ROLES.ANONYMOUS,ROLES.GUEST);
    }
    public static Boolean rolesValidate(String roles, List<String> roleGenerated){
        String[] split = roles.split(",");
        for (String s : split) {
            if(!allRoles.contains(s))
                return false;
        }
        roleGenerated.addAll(Arrays.asList(split));
        return true;
    }
}
