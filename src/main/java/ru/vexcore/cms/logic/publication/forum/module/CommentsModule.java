package ru.vexcore.cms.logic.publication.forum.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumComment;
import ru.vexcore.cms.repository.forum.ForumCommentsRepository;

import java.util.List;

import static ru.vexcore.cms.logic.publication.forum.module.UtilsModule.commentsRemovePosts;

@Service
public class CommentsModule {

    @Autowired
    ForumCommentsRepository commentsRepository;

    @Transactional(readOnly = true)
    public List<ForumComment> getCommentsByPostId(Long postId, Integer startIdnex, Integer len){
        Pageable pages = PageRequest.of(startIdnex, len, Sort.by("date").descending());
        List<ForumComment> allByPostID = commentsRepository.findAllByPostID(postId,pages);
        commentsRemovePosts(allByPostID);
        return allByPostID;
    }

    @Transactional(readOnly = true)
    public List<ForumComment> getCommentsByUserSub(String sub){
        List<ForumComment> allByUserSub = commentsRepository.findAllByUserSub(sub);
        allByUserSub.forEach(x->x.setUserInfo(null));
        return allByUserSub;
    }

    @Transactional
    public ForumComment postComment(UserInfo userInfo, String text, Long id){
        ForumComment forumComments = new ForumComment();
        forumComments.setUserSub(userInfo.getSub());
        forumComments.setPostID(id);
        forumComments.setText(text);
        return commentsRepository.save(forumComments);
    }

    public Integer getCountComments(Long postId){
       return commentsRepository.countAllByPostID(postId);
    }
    public boolean deleteComment(Long id,UserInfo userInfo){
        ForumComment comments = commentsRepository.getById(id);
        if(comments==null||userInfo.getSub().equals(comments.getUserSub()))
            return false;
        commentsRepository.delete(comments);
        return true;
    }
    public boolean changeComment(Long id,String mess,UserInfo userInfo){
        ForumComment comments = commentsRepository.getById(id);
        if(comments==null||userInfo.getSub().equals(comments.getUserSub()))
            return false;
        comments.setText(mess);
        commentsRepository.save(comments);
        return true;
    }



}
