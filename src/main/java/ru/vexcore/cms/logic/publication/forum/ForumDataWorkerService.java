package ru.vexcore.cms.logic.publication.forum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.vexcore.cms.logic.publication.forum.module.CommentsModule;
import ru.vexcore.cms.logic.publication.forum.module.EditorsModule;
import ru.vexcore.cms.logic.publication.forum.module.PostsModule;
import ru.vexcore.cms.logic.publication.forum.module.TopicsModule;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumPost;

import java.util.List;

import static ru.vexcore.cms.logic.Utils.responseGenerator;


@Service
public class ForumDataWorkerService {
    @Autowired
    CommentsModule commentsModule;
    @Autowired
    EditorsModule editorsModule;
    @Autowired
    PostsModule postsModule;
    @Autowired
    TopicsModule topicsModule;

    public ResponseEntity<String> getCommentsByPostId(Long postId, Integer startIndex, Integer len){
        if(startIndex==null)
            startIndex = 0;
        if(len == null)
            len = 10;
       return responseGenerator(commentsModule.getCommentsByPostId(postId,startIndex,len));
    }
    public ResponseEntity<String> getCommentsByUserSub(String sub){
        if(sub==null)
            return responseGenerator(null,403,"not user sub or post id");
        return responseGenerator(commentsModule.getCommentsByUserSub(sub));
    }



    public ResponseEntity<String> getEditorsByUserSub(String sub){
        return responseGenerator(editorsModule.getEditrosByUserSub(sub));
    }
    public ResponseEntity<String> getPostById(Long id, Integer startIndexComment, Integer len){
        ForumPost post = postsModule.getPostById(id,startIndexComment,len);
        if(post==null)
            return responseGenerator(null,404,"post not found");
        return responseGenerator(countCommentsSetter(post));
    }
    public ResponseEntity<String> createPost(String title, String body, UserInfo user, Long topicID){
        ForumPost post = postsModule.createPost(title,body,user,topicID);
        if(post==null)
            return responseGenerator(null,500,"Internal Error, please attach admins");
        return responseGenerator(0);
    }
    public ResponseEntity<String> changePost(Long id,String title, String body,UserInfo info,Long topicId){
        if(body.length()==0)
            body = null;
        if(title.length()==0)
            title = null;
        if(postsModule.changePost(id,title,body,info,topicId))
            return responseGenerator(true);
        return responseGenerator(false,403,"post to change not found, or you have't permissions");
    }
    public ResponseEntity<String> deletePost(Long id,UserInfo info){
        if(postsModule.deltePost(id,info))
            return responseGenerator(true);
        return responseGenerator(false,403,"post to change not found, or you have't permissions");
    }

    public ResponseEntity<String> getPostByAuthor(String sub){
        List<ForumPost> postByAuthor = postsModule.getPostByAuthor(sub);
        postByAuthor.forEach(this::countCommentsSetter);
        return responseGenerator(postByAuthor);
    }
    public ResponseEntity<String> getPostByTopicId(Long topicID){
        return responseGenerator(postsModule.getPostByTopicId(topicID));
    }
    public ResponseEntity<String> getTopicTree(Long rootId){
        return responseGenerator(topicsModule.getTopicTree(rootId));
    }
    public ResponseEntity<String> findTopics(String title){
        return responseGenerator(topicsModule.findTopics(title));
    }


    public ResponseEntity<String> getTopicById(Long id){
        return responseGenerator(topicsModule.getTopicById(id));
    }
    public ResponseEntity<String> getFullTreeOfTopics(){
        return responseGenerator(topicsModule.getFullTreeOfTopics());
    }
    public ResponseEntity<String> changeTopic(Long id, Long root,String name){
        return responseGenerator(topicsModule.changeTopic(id,root,name));
    }
    public ResponseEntity<String> createTopic(String title,Long root){
        return responseGenerator(topicsModule.createTopic(title,root));
    }
    public ResponseEntity<String> deleteTopic(Long id){
        return responseGenerator(topicsModule.deleteTopic(id));
    }

    public ResponseEntity<String>  postComment(UserInfo userInfo, String text, Long id){
        return responseGenerator(commentsModule.postComment(userInfo,text,id));
    }

    public ResponseEntity<String>  deleteComment(Long id,UserInfo info){
        return responseGenerator(commentsModule.deleteComment(id,info));
    }
    public ResponseEntity<String> changeComment(Long id,String mess,UserInfo info){
        return responseGenerator(commentsModule.changeComment(id,mess,info));
    }

    public ResponseEntity<String> getCountCommentsBy(Long postId){
        return responseGenerator(commentsModule.getCountComments(postId));
    }

    private ForumPost countCommentsSetter(ForumPost post){
        post.setCommentsCount(commentsModule.getCountComments(post.getId()));
        return post;
    }



}
