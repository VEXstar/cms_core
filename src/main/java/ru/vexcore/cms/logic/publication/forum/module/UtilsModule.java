package ru.vexcore.cms.logic.publication.forum.module;

import ru.vexcore.cms.logic.role.RoleUtils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumComment;
import ru.vexcore.cms.model.forum.ForumEditor;
import ru.vexcore.cms.model.forum.ForumPost;
import ru.vexcore.cms.model.forum.ForumTopic;

import java.util.List;

public class UtilsModule {
    public static void topicCleanToTop(ForumTopic topic){
        if(topic==null)
            return;
        topic.setChildrens(null);
        topic.setPosts(null);
        topicCleanToTop(topic.getRootTopic());
    }
    public static void topicCleantToDeep(ForumTopic topic){
        if(topic == null)
            return;
        topic.setPosts(null);
        topic.setRootTopic(null);
        topic.getChildrens().forEach(UtilsModule::topicCleantToDeep);
    }
    public static void topicMultiClean(ForumTopic topic){
        postToHeader(topic.getPosts());
        topicCleanToTop(topic.getRootTopic());
        topic.getChildrens().forEach(UtilsModule::topicCleantToDeep);
    }
    public static void generateTreeForTopic(ForumTopic topic){
        postToHeader(topic.getPosts());
        topic.setRootTopic(null);
        topic.getChildrens().forEach(UtilsModule::generateTreeForTopic);
    }
    public static void topicThreadLineGenerate(ForumTopic topic){
        if(topic==null)
            return;
        topic.setPosts(null);
        topic.setChildrens(null);
        topicThreadLineGenerate(topic.getRootTopic());
    }

    public static void commentsRemovePosts(List<ForumComment> comments){
        if(comments!=null)
            comments.forEach(x->x.setPost(null));
    }
    public static List<ForumComment> commentBlocker(List<ForumComment> comments, Integer startIndexComment, Integer len){
        if(startIndexComment>=comments.size())
            return comments;
        if(startIndexComment+len==0)
            return null;
        if(startIndexComment+len>=comments.size())
            comments = comments.subList(startIndexComment,startIndexComment+comments.size());
        else
            comments.subList(startIndexComment,len+startIndexComment);
        return comments;
    }

    public static void postToHeader(ForumPost x){
        x.setBody(null);
        x.setComments(null);
        x.setEditors(null);
        x.setTopic(null);
    }

    public static void postToHeader(List<ForumPost> posts){
        posts.forEach(UtilsModule::postToHeader);
    }


    public static void editorsRemovePosts(List<ForumEditor> editors){
        if(editors!=null)
            editors.forEach(x->x.setPost(null));
    }

    public static boolean isPermited(UserInfo info,List<ForumEditor> allEditors){
        boolean permited = false;
        if(RoleUtils.userIsAdmin(info))
            permited = true;
        else
            for(ForumEditor editors: allEditors)
                if(editors.getUserSub().equals(info.getSub())&&editors.getType().equals("AUTHOR")) {
                    permited = true;
                    break;
                }
            return permited;
    }

}
