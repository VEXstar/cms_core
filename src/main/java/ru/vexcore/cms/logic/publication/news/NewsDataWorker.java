package ru.vexcore.cms.logic.publication.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.role.RoleUtils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.additional.poll.PollCore;
import ru.vexcore.cms.model.news.Blog;
import ru.vexcore.cms.model.news.BlogComment;
import ru.vexcore.cms.model.news.BlogTag;
import ru.vexcore.cms.model.news.BlogTagsChain;
import ru.vexcore.cms.repository.additional.poll.PollCoreRepository;
import ru.vexcore.cms.repository.news.BlogChainRepository;
import ru.vexcore.cms.repository.news.BlogCommentRepository;
import ru.vexcore.cms.repository.news.BlogRepository;
import ru.vexcore.cms.repository.news.BlogTagRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewsDataWorker {


    @Autowired
    BlogRepository blogRepository;
    @Autowired
    BlogTagRepository blogTagRepository;
    @Autowired
    BlogCommentRepository commentRepository;

    @Autowired
    PollCoreRepository pollCoreRepository;

    @Autowired
    BlogChainRepository blogChainRepository;

    @Transactional(readOnly = true)
    public ResponseEntity<String> getBlogs(Integer page){
        Pageable pages = PageRequest.of(page, 10, Sort.by("date").descending());
        List<Blog> blogs = blogRepository.findAll(pages);
        blogs.forEach(x->x.getTags().forEach(y->y.setBlogs(null)));
        return Utils.responseGenerator(blogs);
    }
    @Transactional(readOnly = true)
    public ResponseEntity<String> getBlogById(Long id){
        Blog blog = blogRepository.getBlogById(id);
        return Utils.responseGenerator(blog);
    }
    @Transactional(readOnly = true)
    public ResponseEntity<String> findAllBlogsByTerm(String term){
        List<Blog> blogs = blogRepository.findAllByBodyContains(term);
        return Utils.responseGenerator(blogs);
    }

    @Transactional
    public ResponseEntity<String> createBlog(String body, UserInfo info,List<String> tags){
        Blog blog = new Blog();
        blog.setBody(body);
        blog.setSub(info.getSub());

        PollCore pollCore = new PollCore();
        pollCore.setLike(true);

        blog.setPollId(pollCoreRepository.save(pollCore).getId());
        Blog save = blogRepository.save(blog);
        handleTags(tags,save.getId());
        return Utils.responseGenerator(save);
    }

    private void handleTags(List<String> tags,Long blogId){
        List<BlogTagsChain> tagsChains = new ArrayList<>();
        tags.forEach(x->{
            BlogTag byName = blogTagRepository.getByName(x);
            if(byName==null)
             byName = blogTagRepository.save(new BlogTag(x));
            tagsChains.add(new BlogTagsChain(blogId,byName.getId()));
        });
        blogChainRepository.saveAll(tagsChains);
    }

    @Transactional
    public ResponseEntity<String> changeBlog(Long id,String body, UserInfo info,List<String> tags){
        Blog blog = blogRepository.getBlogById(id);
        if(blog==null||!(blog.getSub().equals(info.getSub())||RoleUtils.userIsAdmin(info)))
            return Utils.responseGenerator(false,400,"blog not found or user hasn't author");
        if(body!=null)
            blog.setBody(body);
        if(tags!=null){
            List<BlogTagsChain> tagsChains = blogChainRepository.findAllByBlogId(blog.getId());
            blogChainRepository.deleteAll(tagsChains);
            handleTags(tags,blog.getId());
        }
        return Utils.responseGenerator(true);
    }
    @Transactional
    public ResponseEntity<String> deleteBlog(Long id,UserInfo info){
        Blog blog = blogRepository.getBlogById(id);
        if(blog==null||!(blog.getSub().equals(info.getSub())||RoleUtils.userIsAdmin(info)))
            return Utils.responseGenerator(false,400,"blog not found or user hasn't author");
        pollCoreRepository.delete(blog.getLikes());
        blogRepository.delete(blog);
        return Utils.responseGenerator(true);
    }

    public ResponseEntity<String> createComment(Long idRoot,Boolean isBlog,String body,UserInfo info){
        BlogComment comment = new BlogComment(idRoot,body,info.getSub(),isBlog);
        return   Utils.responseGenerator(commentRepository.save(comment));
    }

    @Transactional
    public ResponseEntity<String> changeComment(Long id,String body,UserInfo info){
        BlogComment comment = commentRepository.getById(id);
        if(comment==null||body==null||!comment.getSub().equals(info.getSub()))
            return Utils.responseGenerator(false,400,"Comment not found or user has't author or not param body");
        comment.setBody(body);
        return   Utils.responseGenerator(true);
    }
    @Transactional
    public ResponseEntity<String> deleteComment(Long id,UserInfo info){
        BlogComment comment = commentRepository.getById(id);
        if(comment==null||!(comment.getSub().equals(info.getSub())||RoleUtils.userIsAdmin(info)))
            return Utils.responseGenerator(false,403,"Comment not found or user has't author");
        commentRepository.delete(comment);
        return   Utils.responseGenerator(true);
    }

    @Transactional
    public ResponseEntity<String> getTagsByTerm(String term){
        if(term==null)
            term = "";
        List<BlogTag> tags = blogTagRepository.findAllByNameContaining(term);
        tags.forEach(x->x.getBlogs().forEach(y->y.setTags(null)));
        return Utils.responseGenerator(tags);
    }






}
