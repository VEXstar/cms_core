package ru.vexcore.cms.logic.publication.forum.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vexcore.cms.model.forum.ForumTopic;
import ru.vexcore.cms.repository.forum.ForumTopicRepository;

import java.util.List;
import java.util.Map;

import static ru.vexcore.cms.logic.publication.forum.module.UtilsModule.topicMultiClean;

@Service
public class TopicsModule {

    @Autowired
    ForumTopicRepository forumTopicRepository;

    @Transactional(readOnly = true)
    public ForumTopic getTopicTree(Long rootId){
        ForumTopic topic = forumTopicRepository.getById(rootId);
        if(topic==null)
            return null;
        topicMultiClean(topic);
        return topic;
    }
    @Transactional(readOnly = true)
    public List<ForumTopic> findTopics(String title){
        List<ForumTopic> topics = forumTopicRepository.findAllByTitleContaining(title);
        topics.forEach(UtilsModule::topicMultiClean);
        return topics;
    }

    @Transactional(readOnly = true)
    public List<ForumTopic> getRootTopics(){
        List<ForumTopic> allByRootTopicId = forumTopicRepository.getAllByRootTopicIdIsNull();
        allByRootTopicId.forEach(UtilsModule::topicMultiClean);
        return allByRootTopicId;
    }
    @Transactional(readOnly = true)
    public ForumTopic getTopicById(Long id){
        ForumTopic topic = forumTopicRepository.getById(id);
        if(topic==null)
            return null;
        topicMultiClean(topic);
        return topic;
    }

    @Transactional(readOnly = true)
    public List<ForumTopic> getFullTreeOfTopics(){
        List<ForumTopic> allByRootTopicId = forumTopicRepository.getAllByRootTopicIdIsNull();
        allByRootTopicId.forEach(UtilsModule::generateTreeForTopic);
        return allByRootTopicId;
    }

    @Transactional
    public boolean changeTopic(Long id, Long root,String name){
        ForumTopic topic = forumTopicRepository.getById(id);
        if(topic==null)
            return false;
        if(root != null)
            topic.setRootTopicId(root);
        if(name!=null)
            topic.setTitle(name);
        forumTopicRepository.save(topic);
        return true;
    }

    public ForumTopic createTopic(String title,Long root){
        ForumTopic topic = new ForumTopic();
        topic.setTitle(title);
        topic.setRootTopicId(root);
        return forumTopicRepository.save(topic);
    }

    public boolean deleteTopic(Long id){
        ForumTopic topic = forumTopicRepository.getById(id);
        if(topic==null)
            return false;
        forumTopicRepository.delete(topic);
        return true;
    }


}
