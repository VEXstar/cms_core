package ru.vexcore.cms.logic.publication.forum.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vexcore.cms.model.forum.ForumEditor;
import ru.vexcore.cms.repository.forum.ForumChainPostUserRepository;

import java.util.List;

import static ru.vexcore.cms.logic.publication.forum.module.UtilsModule.postToHeader;

@Service
public class EditorsModule {

    @Autowired
    ForumChainPostUserRepository chainPostUserRepository;

    @Transactional(readOnly = true)
    public List<ForumEditor> getEditrosByUserSub(String sub){
        List<ForumEditor> allByUserSub = chainPostUserRepository.getAllByUserSub(sub);
        allByUserSub.forEach(x->{x.setUserInfo(null);postToHeader(x.getPost());});
        return allByUserSub;
    }
    public List<ForumEditor> getByPostId(Long postID){
        List<ForumEditor> allByPostID = chainPostUserRepository.getAllByPostID(postID);
        allByPostID.forEach(x->x.setPost(null));
        return allByPostID;
    }
}
