package ru.vexcore.cms.logic.publication.forum.module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumEditor;
import ru.vexcore.cms.model.forum.ForumPost;
import ru.vexcore.cms.repository.forum.ForumChainPostUserRepository;
import ru.vexcore.cms.repository.forum.ForumPostRepository;

import java.util.List;
import java.util.stream.Collectors;

import static ru.vexcore.cms.logic.publication.forum.module.UtilsModule.*;

@Service
public class PostsModule {

    @Autowired
    ForumPostRepository forumPostRepository;
    @Autowired
    ForumChainPostUserRepository chainPostUserRepository;


    @Transactional(readOnly = true)
    public ForumPost getPostById(Long id,Integer startIndexComment,Integer len){
        ForumPost post = forumPostRepository.getById(id);
        if(post==null)
            return null;
        topicThreadLineGenerate(post.getTopic());
        post.setComments(commentBlocker(post.getComments(),startIndexComment,len));
        commentsRemovePosts(post.getComments());
        editorsRemovePosts(post.getEditors());
        return post;
    }

    public ForumPost createPost(String title, String body, UserInfo user, Long topicID){
        ForumPost forumPost = new ForumPost();
        forumPost.setBody(body);
        forumPost.setTitle(title);
        forumPost.setTopicId(topicID);
        ForumPost save = forumPostRepository.save(forumPost);
        ForumEditor editors = new ForumEditor();
        editors.setPostID(save.getId());
        editors.setUserSub(user.getSub());
        chainPostUserRepository.save(editors);
        return save;
    }

    @Transactional
    public boolean changePost(Long id,String title, String body,UserInfo info,Long topicId){
        ForumPost post = forumPostRepository.getById(id);
        if(post==null)
            return false;


        if(!isPermited(info,post.getEditors()))
            return false;
        if(body!=null)
            post.setBody(body);
        if(title!=null)
            post.setTitle(title);
        if(topicId!=null)
            post.setTopicId(topicId);
        forumPostRepository.save(post);
        ForumEditor editors = new ForumEditor();
        editors.setPostID(post.getId());
        editors.setUserSub(info.getSub());
        editors.setType("REDACTOR");
        chainPostUserRepository.save(editors);
        return true;
    }

    public boolean deltePost(Long id,UserInfo info){
        ForumPost post = forumPostRepository.getById(id);
        if(post==null)
            return false;
        if(!isPermited(info,post.getEditors()))
            return false;
        forumPostRepository.delete(post);
        return true;
    }
    @Transactional(readOnly = true)
    public List<ForumPost> getPostByAuthor(String sub){
        List<ForumEditor> allByUserSub = chainPostUserRepository.getAllByUserSub(sub);
        allByUserSub.forEach(x->{
            ForumPost post = x.getPost();
            post.setTopic(null);
            post.setEditors(null);
            post.setComments(null);
        });
        return allByUserSub.stream().map(ForumEditor::getPost).collect(Collectors.toList());
    }
    @Transactional(readOnly = true)
    public List<ForumPost> getPostByTopicId(Long topicID){
        List<ForumPost> allByTopic = forumPostRepository.findAllByTopicIdAndOrderByPostedTimeDesc(topicID);
        postToHeader(allByTopic);
        return allByTopic;
    }
}
