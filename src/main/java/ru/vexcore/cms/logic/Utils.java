package ru.vexcore.cms.logic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import ru.vexcore.cms.model.ResponseModel;
import ru.vexcore.cms.model.UserInfo;

import java.sql.Timestamp;
import java.util.Date;

public class Utils {
    private static ObjectMapper objectMapper = new ObjectMapper();
    public static String ToJSON(Object e){
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return objectMapper.writeValueAsString(e);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
    public static UserInfo getUserInfo(Authentication authentication){
        return ((DefaultOidcUser)authentication.getPrincipal()).getUserInfo().getClaim("data");
    }

    public static ResponseEntity<String> responseGenerator(Object element, Integer code, String error){
        if(code==null)
            code = 200;
        return ResponseEntity.status(code).body(Utils.ToJSON(new ResponseModel(code,error,element)));
    }
    public static ResponseEntity<String> responseGenerator(Object element){
        return ResponseEntity.ok(Utils.ToJSON(new ResponseModel(200,null,element)));
    }
    public static Timestamp nowTimeStamp(){
       return new Timestamp(new Date().getTime());
    }
}
