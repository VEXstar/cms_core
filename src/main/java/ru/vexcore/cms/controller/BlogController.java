package ru.vexcore.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.publication.news.NewsDataWorker;

import java.util.Arrays;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/api/news", produces = "text/plain;charset=UTF-8")
public class BlogController {
    @Autowired
    NewsDataWorker newsDataWorker;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    ResponseEntity<String> getnew(Authentication authentication,@RequestParam("page") Integer page,@RequestParam("term") String term){
        if(term!=null)
            return newsDataWorker.findAllBlogsByTerm(term);
        if(page==null)
            page=0;
        return newsDataWorker.getBlogs(page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ResponseEntity<String> getCnewById(Authentication authentication,@PathVariable("id") Long id){
        return newsDataWorker.getBlogById(id);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole(RoleUtils.canPublicateNews())")
    ResponseEntity<String> createNew(Authentication authentication,@RequestParam("body") String body,@RequestParam("tags") String tags){
        return newsDataWorker.createBlog(body, Utils.getUserInfo(authentication), Arrays.asList(tags.split(",")));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole(RoleUtils.canPublicateNews())")
    ResponseEntity<String> updateNew(Authentication authentication,@PathVariable("id") Long id,@RequestParam("body") String body,@RequestParam("tags") String tags){
        return newsDataWorker.changeBlog(id,body, Utils.getUserInfo(authentication), Arrays.asList(tags.split(",")));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole(RoleUtils.canPublicateNews())")
    ResponseEntity<String> deleteNew(Authentication authentication,@PathVariable("id") Long id){
        return newsDataWorker.deleteBlog(id,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @PreAuthorize("!hasAnyRole(RoleUtils.isAnonymous())")
    ResponseEntity<String> createComment(Authentication authentication,@RequestParam("body") String body,@RequestParam("root") Long root,@RequestParam("blog") Boolean isBlog){
        return newsDataWorker.createComment(root,isBlog,body,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/comment/{id}", method = RequestMethod.PUT)
    @PreAuthorize("!hasAnyRole(RoleUtils.isAnonymous())")
    ResponseEntity<String> changeComment(Authentication authentication,@PathVariable("id") Long id,@RequestParam("body") String body){
        return newsDataWorker.changeComment(id,body,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/comment/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("!hasAnyRole(RoleUtils.isAnonymous())")
    ResponseEntity<String> deleteComment(Authentication authentication,@PathVariable("id") Long id){
        return newsDataWorker.deleteComment(id,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/tag", method = RequestMethod.GET)
    ResponseEntity<String> deleteComment(Authentication authentication,@RequestParam("term") String  term){
        return newsDataWorker.getTagsByTerm(term);
    }

}
