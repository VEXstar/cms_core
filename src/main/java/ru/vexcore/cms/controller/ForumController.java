package ru.vexcore.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.publication.forum.ForumDataWorkerService;


@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/api/forum", produces = "text/plain;charset=UTF-8")
public class ForumController {

    @Autowired
    ForumDataWorkerService forumDataWorkerService;



    @RequestMapping(value = "/comment", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole(@RoleUtils.isViewForum())")
    ResponseEntity<String> getComment(@RequestParam("post") Long id,@RequestParam("from") Integer from,@RequestParam("len") Integer len, @RequestParam("sub") String sub){
        if(id!=null)
            return forumDataWorkerService.getCommentsByUserSub(sub);
        return forumDataWorkerService.getCommentsByPostId(id,from,len);
    }

    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> postComment(Authentication authentication,@RequestParam("post") Long id, @RequestParam("text") String text){
        return forumDataWorkerService.postComment(Utils.getUserInfo(authentication),text,id);
    }

    @RequestMapping(value = "/comment/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> changeComment(Authentication authentication, @PathVariable("id") Long id, @RequestParam("text") String text){
        return forumDataWorkerService.changeComment(id,text,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/comment/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> deleteComment(Authentication authentication,@PathVariable("id") Long id){
        return forumDataWorkerService.deleteComment(id,Utils.getUserInfo(authentication));
    }



    @RequestMapping(value = "/topic/{id}",method = RequestMethod.GET)
    ResponseEntity<String> getTopicById(Authentication authentication,@PathVariable("id") Long id){
        return forumDataWorkerService.getTopicById(id);
    }
    @RequestMapping(value = "/topic",method = RequestMethod.GET)
    ResponseEntity<String> findTopic(Authentication authentication,@RequestParam("term") String term){
        return forumDataWorkerService.findTopics(term);
    }

    @RequestMapping(value = "/topic/tree", method = RequestMethod.GET)
    ResponseEntity<String> getTopicByTerm(Authentication authentication,@RequestParam("id") Long id){
       if(id!=null)
           return forumDataWorkerService.getTopicTree(id);
        return forumDataWorkerService.getFullTreeOfTopics();
    }

    @RequestMapping(value = "/topic",method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole(@RoleUtils.canModerateForum())")
    ResponseEntity<String> postTopic(Authentication authentication,@RequestParam("name") String title,@RequestParam("root") Long rootId){
      return forumDataWorkerService.createTopic(title,rootId);
    }

    @RequestMapping(value = "/topic",method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole(@RoleUtils.canModerateForum())")
    ResponseEntity<String> changeTopic(Authentication authentication,@RequestParam("id") Long id,@RequestParam("name") String title,@RequestParam("root") Long rootId){
        return forumDataWorkerService.changeTopic(id,rootId,title);
    }

    @RequestMapping(value = "/topic",method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole(@RoleUtils.canModerateForum())")
    ResponseEntity<String> deleteTopic(Authentication authentication,@RequestParam("id") Long id){
        return forumDataWorkerService.deleteTopic(id);
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole(@RoleUtils.isViewForum())")
    ResponseEntity<String> getPostById(Authentication authentication,@PathVariable("id") Long id,@RequestParam("c_from") Integer from,@RequestParam("c_len") Integer len,@RequestParam("topic") Long topic,@RequestParam("sub") String sub){
        if(id!=null)
            return forumDataWorkerService.getPostById(id,from,len);
        if(topic!=null)
            return forumDataWorkerService.getPostByTopicId(topic);
        if(sub!=null)
            return forumDataWorkerService.getPostByAuthor(sub);
        else
            return forumDataWorkerService.getPostById(id,from,len);
    }

    @RequestMapping(value = "/post",method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> changePost(Authentication authentication,@RequestParam("id") Long id,@RequestParam("title") String title,@RequestParam("body") String body,@RequestParam("topic") Long topic){
        return forumDataWorkerService.changePost(id,title,body,Utils.getUserInfo(authentication),topic);
    }
    @RequestMapping(value = "/post",method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> createPost(Authentication authentication,@RequestParam("title") String title,@RequestParam("body") String body,@RequestParam("topic") Long topic){
        return forumDataWorkerService.createPost(title,body,Utils.getUserInfo(authentication),topic);
    }

    @RequestMapping(value = "/post/{id}",method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole(@RoleUtils.canWtiteForum())")
    ResponseEntity<String> deletePost(Authentication authentication,@PathVariable("id") Long id){
        return forumDataWorkerService.deletePost(id,Utils.getUserInfo(authentication));
    }

    @RequestMapping(value = "/editor/{sub}", method = RequestMethod.GET)
    ResponseEntity<String> getEditorsBySub(Authentication authentication,@PathVariable("sub") String sub){
        return forumDataWorkerService.getEditorsByUserSub(sub);
    }




    //TODO: Добавить остальные контроллеры

}
