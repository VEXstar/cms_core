package ru.vexcore.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.*;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.user.UserDataWorkerService;
import ru.vexcore.cms.model.UserInfo;

import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/api/user", produces = "text/plain;charset=UTF-8")
public class UserDataController {

    @Autowired
    UserDataWorkerService userDataWorkerService;

    @RequestMapping(value = "/{user}", method = RequestMethod.GET)
    @PreAuthorize("!hasAnyRole(RoleUtils.isAnonymous())")
    ResponseEntity<String> info(Authentication authentication,@PathVariable("user") String user){
        if(user != null)
            return userDataWorkerService.getBothUser(user);
        else
            return Utils.responseGenerator(((DefaultOidcUser)authentication.getPrincipal()).getUserInfo().getClaim("data"));
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @PreAuthorize("!hasAnyRole(RoleUtils.isAnonymous())")
    ResponseEntity<String> change(Authentication authentication, @RequestParam("nick") String nick, @RequestParam("avatar") String avatar){
        UserInfo fromSession = ((DefaultOidcUser)authentication.getPrincipal()).getUserInfo().getClaim("data");
        return userDataWorkerService.updateDataUser(fromSession.getSub(),nick,avatar);
    }




    @RequestMapping(value = "/role", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole(RoleUtils.adminRoles())")
    ResponseEntity<String> setRoles(Authentication authentication, @RequestParam("roles") String roles,@RequestParam("sub") String sub){
       return  userDataWorkerService.editRoles(sub,UserDataWorkerService.ROLE_ACTION.SET,roles);
    }

    @RequestMapping(value = "/role", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole(RoleUtils.adminRoles())")
    ResponseEntity<String> updateRoles(Authentication authentication, @RequestParam("roles") String roles,@RequestParam("sub") String sub){
        return userDataWorkerService.editRoles(sub,UserDataWorkerService.ROLE_ACTION.UPDATE,roles);
    }

    @RequestMapping(value = "/role", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole(RoleUtils.adminRoles())")
    ResponseEntity<String> deleteRoles(Authentication authentication, @RequestParam("roles") String roles,@RequestParam("sub") String sub){
        return userDataWorkerService.editRoles(sub,UserDataWorkerService.ROLE_ACTION.DELETE,roles);
    }


}
