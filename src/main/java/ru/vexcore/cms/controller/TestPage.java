package ru.vexcore.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.logic.publication.forum.ForumDataWorkerService;
import ru.vexcore.cms.logic.user.UserDataWorkerService;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.repository.UserInfoRepository;

import java.util.*;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/test", produces = "text/plain;charset=UTF-8",method = RequestMethod.GET)
public class TestPage {

    @Autowired
    UserInfoRepository repository;
    @Autowired
    UserDataWorkerService manager;
    @Autowired
    ForumDataWorkerService forumDataWorker;




//    @RequestMapping("/role")
//    ResponseEntity<String> updR (@RequestParam("q") String q,Authentication authentication){
//        UserInfo fromSession = Utils.getUserInfo(authentication);
//        List<String> antiRoles = new ArrayList<>();
//        antiRoles.add(q);
//        manager.editRoles(fromSession.getSub(), UserDataWorkerService.ROLE_ACTION.UPDATE,antiRoles);
//        return ResponseEntity.ok("OK");
//    }
//    @RequestMapping("/nick")
//    ResponseEntity<String> upd (@RequestParam("q") String q,Authentication authentication){
//        UserInfo fromSession = Utils.getUserInfo(authentication);
//        Map<String ,String> req = new HashMap<>();
//        req.put("nick_name",q);
//        manager.updateDataUser(fromSession.getSub(),req);
//        return ResponseEntity.ok("OK");
//    }


    @RequestMapping("/topic")
    ResponseEntity<String> testTopics (){
        String ret = "";
//        List<ForumTopic> rootTopics = forumDataWorker.getRootTopics();
        String s = "A";
//        ret+= Utils.ToJSON(rootTopics)+"\n\n";
//        ret+= Utils.ToJSON(forumDataWorker.getTopicTree(rootTopics.get(0).getChildrens().iterator().next().getId()))+"\n\n";
        ret+= Utils.ToJSON(forumDataWorker.findTopics("2"))+"\n\n";
        ret+= Utils.ToJSON(forumDataWorker.getFullTreeOfTopics())+"\n\n";
        ret+= Utils.ToJSON(forumDataWorker.getTopicById(4L));
        return ResponseEntity.ok(ret+s);
    }

    @RequestMapping("/post")
    ResponseEntity<String> testPost (Authentication authentication){
        UserInfo userInfo = Utils.getUserInfo(authentication);
        String ret = "";
//        ForumPost post = forumDataWorker.createPost("GeneratedPost2", "FUCK U", userInfo, 2L);
//        ret+= "NEW POST: "+Utils.ToJSON(post)+"\n\n";
//        ret+= "EDITED? :"+Utils.ToJSON(forumDataWorker.changePost(post.getId(),null,"AHAHAHA NO!",userInfo,null,false))+"\n\n";
//        ret+= "NEW POST :"+Utils.ToJSON(forumDataWorker.getPostById(post.getId(),0,10))+"\n\n";
//        ret+= "YOUR POSTS: "+Utils.ToJSON(forumDataWorker.getPostByAuthor(userInfo.getSub()))+"\n\n";
        return ResponseEntity.ok(ret);
    }

}
