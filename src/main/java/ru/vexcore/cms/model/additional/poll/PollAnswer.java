package ru.vexcore.cms.model.additional.poll;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "poll_answer")
public class PollAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String sub;

    @Column(name = "check_id")
    private Long checkID;

    @Column(name = "poll_id")
    private Long pollId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "poll_id", nullable = false,insertable = false,updatable = false)
    @JsonIgnore
    PollCore pollCore;

    @ManyToOne
    @JoinColumn(name = "check_id", nullable = true,insertable = false,updatable = false)
    @JsonIgnore
    PollCheck pollCheck;

    public PollAnswer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public Long getCheckID() {
        return checkID;
    }

    public void setCheckID(Long checkID) {
        this.checkID = checkID;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public PollCore getPollCore() {
        return pollCore;
    }

    public void setPollCore(PollCore pollCore) {
        this.pollCore = pollCore;
    }

    public PollCheck getPollCheck() {
        return pollCheck;
    }

    public void setPollCheck(PollCheck pollCheck) {
        this.pollCheck = pollCheck;
    }
}
