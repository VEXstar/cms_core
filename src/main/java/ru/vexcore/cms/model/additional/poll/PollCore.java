package ru.vexcore.cms.model.additional.poll;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "poll")
public class PollCore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String body;

    private Timestamp expire;

    private Boolean like;

    @OneToMany(mappedBy = "pollCore")
    private HashSet<PollCheck> checks;

    @OneToMany(mappedBy = "pollCore",fetch = FetchType.EAGER)
    private HashSet<PollAnswer> answers;

    private Map<Long,Integer> statistic= new HashMap<>();

    public Map<Long, Integer> getStatistic() {
        return statistic;
    }
    public void incrementStatistic(Long checkID){
        if(statistic.containsKey(checkID))
            statistic.replace(checkID,statistic.get(checkID)+1);
        else
            statistic.put(checkID,1);
    }

    public void setStatistic(Map<Long, Integer> statistic) {
        this.statistic = statistic;
    }

    public PollCore() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }



    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getExpire() {
        return expire;
    }

    public void setExpire(Timestamp expire) {
        this.expire = expire;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }

    public HashSet<PollCheck> getChecks() {
        return checks;
    }

    public void setChecks(HashSet<PollCheck> checks) {
        this.checks = checks;
    }

    public HashSet<PollAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(HashSet<PollAnswer> answers) {
        this.answers = answers;
    }
}
