package ru.vexcore.cms.model.forum;

import ru.vexcore.cms.model.UserInfo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "forum_comments")
public class ForumComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "body")
    private String text;

    @Column(name = "date")
    Timestamp postedTime = new Timestamp(new Date().getTime());

    @Column(name = "user_sub")
    private String userSub;

    @Column(name = "forum_post_id")
    private Long postID;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "forum_post_id",nullable = false,insertable = false,updatable = false)
    ForumPost post;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_sub",nullable = true,insertable = false,updatable = false)
    UserInfo userInfo;

    public void setPost(ForumPost post) {
        this.post = post;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(Timestamp postedTime) {
        this.postedTime = postedTime;
    }

    public String getUserSub() {
        return userSub;
    }

    public void setUserSub(String userSub) {
        this.userSub = userSub;
    }

    public Long getPostID() {
        return postID;
    }

    public void setPostID(Long postID) {
        this.postID = postID;
    }

    public ForumPost getPost() {
        return post;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }


    public ForumComment() {
    }
}
