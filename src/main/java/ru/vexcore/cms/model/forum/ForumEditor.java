package ru.vexcore.cms.model.forum;

import ru.vexcore.cms.model.UserInfo;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "forum_post_editors")
public class ForumEditor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "forum_post_id")
    private Long postID;

    @Column(name = "user_sub")
    private String userSub;

    @Column(name = "type")
    private String type = "AUTHOR";

    @Column(name = "edited")
    private Timestamp editedTime = new Timestamp(new Date().getTime());

    @ManyToOne
    @JoinColumn(name = "forum_post_id", nullable = false,insertable = false,updatable = false)
    private ForumPost post;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_sub",nullable = true,insertable = false,updatable = false)
    UserInfo userInfo;

    public Long getPostID() {
        return postID;
    }

    public String getUserSub() {
        return userSub;
    }

    public String getType() {
        return type;
    }

    public Timestamp getEditedTime() {
        return editedTime;
    }

    public ForumPost getPost() {
        return post;
    }

    public void setPost(ForumPost post) {
        this.post = post;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public ForumEditor() {
    }

    public void setPostID(Long postID) {
        this.postID = postID;
    }

    public void setUserSub(String userSub) {
        this.userSub = userSub;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
