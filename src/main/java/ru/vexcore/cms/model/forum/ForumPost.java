package ru.vexcore.cms.model.forum;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "forum_post")
public class ForumPost {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "body")
    private String body;

    @Column(name = "topic_id")
    private Long topicId;

    @Column(name = "date")
    private Timestamp postedTime = new Timestamp(new Date().getTime());

    public Integer getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(Integer commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Integer commentsCount;


    @ManyToOne
    @JoinColumn(name = "topic_id", nullable = false,insertable = false,updatable = false)
    private ForumTopic topic;

    @OneToMany(mappedBy = "post",fetch = FetchType.EAGER)
    private List<ForumEditor> editors;

    @OneToMany(mappedBy = "post")
    private List<ForumComment> comments;

    public List<ForumComment> getComments() {
        return comments;
    }

    public void setEditors(List<ForumEditor> editors) {
        this.editors = editors;
    }

    public void setComments(List<ForumComment> comments) {
        this.comments = comments;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public ForumTopic getTopic() {
        return topic;
    }

    public void setTopic(ForumTopic topic) {
        this.topic = topic;
    }

    public List<ForumEditor> getEditors() {
        return editors;
    }

    public Timestamp getPostedTime() {
        return postedTime;
    }

    public Long getId() {
        return id;
    }

    public ForumPost() {
    }
}
