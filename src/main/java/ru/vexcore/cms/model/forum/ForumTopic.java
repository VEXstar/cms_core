package ru.vexcore.cms.model.forum;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "forum_topic")
public class ForumTopic{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "root")
    private Long rootTopicId;

    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "rootTopic", fetch = FetchType.EAGER)
    private Set<ForumTopic> childrens;

    @OneToMany(mappedBy = "topic")
    private List<ForumPost> posts;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "root", nullable = true,insertable = false,updatable = false)
    private ForumTopic rootTopic;

    public ForumTopic() {
    }

    public Long getRootTopicId() {
        return rootTopicId;
    }

    public void setRootTopicId(Long rootTopicId) {
        this.rootTopicId = rootTopicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<ForumTopic> getChildrens() {
        return childrens;
    }

    public void setChildrens(Set<ForumTopic> childrens) {
        this.childrens = childrens;
    }

    public List<ForumPost> getPosts() {
        return posts;
    }

    public void setPosts(List<ForumPost> posts) {
        this.posts = posts;
    }

    public ForumTopic getRootTopic() {
        return rootTopic;
    }

    public void setRootTopic(ForumTopic rootTopic) {
        this.rootTopic = rootTopic;
    }

    public Long getId() {
        return id;
    }

}
