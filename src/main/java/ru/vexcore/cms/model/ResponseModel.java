package ru.vexcore.cms.model;

public class ResponseModel {
    public int code;
    public String error;
    public Object content;

    public ResponseModel(int code, String error, Object content) {
        this.code = code;
        this.error = error;
        this.content = content;
    }

    public int getCode() {
        return code;
    }

    public String getError() {
        return error;
    }

    public Object getContent() {
        return content;
    }
}
