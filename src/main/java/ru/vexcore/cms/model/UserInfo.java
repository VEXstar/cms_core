package ru.vexcore.cms.model;
import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;


@TypeDefs({
        @TypeDef(
                name = "string-array",
                typeClass = StringArrayType.class
        ),
        @TypeDef(
                name = "int-array",
                typeClass = IntArrayType.class
        )
})


@Entity
@Table(name = "employee")
public class UserInfo{
    @Id
//    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "sub")
    private String sub;
    @Column(name = "nick_name")
    private String nickName;
    @Column(name = "name")
    private String name;
    @Column(name = "avatar")
    private String avatar;
    @Column(name = "email")
    private String email;
    @Type(type = "string-array")
    @Column(name = "authory")
    private String[] roles;

    public String getSub() {
        return sub;
    }

    public String getNickName() {
        return nickName;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getRoles() {
        return Arrays.asList(roles);
    }
    public void removeRoles(List<String> newRoles){
        newRoles.forEach(x->ArrayUtils.removeElement(roles,x));
    }

    public void addRoles(List<String> newRoles){
      roles = (String[]) ArrayUtils.addAll(roles,newRoles.toArray());
    }

    public UserInfo(String sub, String nickName, String name, String avatar, String email, List<String> roles) {
        this.sub = sub;
        this.nickName = nickName;
        this.name = name;
        this.avatar = avatar;
        this.email = email;
        this.roles = new String[roles.size()];
        this.roles = roles.toArray(this.roles);
    }

    public UserInfo() {
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setRoles(List<String> roles) {
        this.roles = new String[roles.size()];
        this.roles = roles.toArray(this.roles);
    }
}
