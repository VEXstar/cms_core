package ru.vexcore.cms.model.news;

import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.additional.poll.PollCore;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "blog")
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String body;

    private Timestamp date = Utils.nowTimeStamp();


    private String sub;

    @Column(name = "poll_id")
    private Long pollId;

    @ManyToMany
    @JoinTable(
            name = "blog_tags",
            joinColumns = @JoinColumn(name = "blog_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private HashSet<BlogTag> tags;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sub",nullable = true,insertable = false,updatable = false)
    private UserInfo userInfo;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "poll_id",nullable = true,insertable = false,updatable = false)
    private PollCore likes;

    public PollCore getLikes() {
        return likes;
    }

    public void setLikes(PollCore likes) {
        this.likes = likes;
    }

    @OneToMany(mappedBy = "blog")
    private List<BlogComment> comments;

    public Blog() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public HashSet<BlogTag> getTags() {
        return tags;
    }

    public void setTags(HashSet<BlogTag> tags) {
        this.tags = tags;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<BlogComment> getComments() {
        return comments;
    }

    public void setComments(List<BlogComment> comments) {
        this.comments = comments;
    }
}
