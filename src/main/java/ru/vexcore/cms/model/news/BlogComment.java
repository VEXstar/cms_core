package ru.vexcore.cms.model.news;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.vexcore.cms.logic.Utils;
import ru.vexcore.cms.model.UserInfo;
import ru.vexcore.cms.model.forum.ForumComment;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "blog_comment")
public class BlogComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "post_id")
    private Long postId;

    @Column(name = "comment_id")
    private Long commentId;

    private String body;

    private Timestamp date = Utils.nowTimeStamp();

    private String sub;

    public BlogComment(Long idRoot, String body, String sub,Boolean isBlog) {
        if(isBlog)
            this.postId = idRoot;
        else
            this.commentId = idRoot;
        this.body = body;
        this.sub = sub;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sub",nullable = true,insertable = false,updatable = false)
    private UserInfo userInfo;

    @ManyToOne
    @JoinColumn(name = "post_id")
    @JsonIgnore
    private Blog blog;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    @JsonIgnore
    private BlogComment comment;

    @OneToMany(mappedBy = "comment")
    private List<BlogComment> subComments;

    public BlogComment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public BlogComment getComment() {
        return comment;
    }

    public void setComment(BlogComment comment) {
        this.comment = comment;
    }

    public List<BlogComment> getSubComments() {
        return subComments;
    }

    public void setSubComments(List<BlogComment> subComments) {
        this.subComments = subComments;
    }
}
