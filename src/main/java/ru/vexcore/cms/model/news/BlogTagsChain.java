package ru.vexcore.cms.model.news;

import javax.persistence.*;

@Entity
@Table(name = "blog_tags")
public class BlogTagsChain {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "blog_id")
    private Long blogId;

    @Column(name = "tag_id")
    private Long tagId;

    public BlogTagsChain() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogId() {
        return blogId;
    }

    public void setBlogId(Long blogId) {
        this.blogId = blogId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public BlogTagsChain(Long blogId, Long tagId) {
        this.blogId = blogId;
        this.tagId = tagId;
    }

}
