package ru.vexcore.cms.model.news;

import javax.persistence.*;
import java.util.HashSet;

@Entity
@Table(name = "blog_tag")
public class BlogTag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany
    @JoinTable(
            name = "blog_tags",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "blog_id"))
    private HashSet<Blog> blogs;

    public BlogTag() {
    }

    public Long getId() {
        return id;
    }

    public BlogTag(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashSet<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(HashSet<Blog> blogs) {
        this.blogs = blogs;
    }
}
