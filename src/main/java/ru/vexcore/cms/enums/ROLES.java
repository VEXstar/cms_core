package ru.vexcore.cms.enums;

public enum  ROLES {
    ADMIN,SYSADMIN,OPERATOR,USER,GUEST,GONID,PUBLICATOR,MODERATOR,ANONYMOUS;

    @Override
    public String toString() {
        return this.name();
    }
}
